import React, {Component} from 'react';
import List from "../List/List";
import Info from "../Info/Info";


class Country  extends Component{
        state = {
            countries: [],
            information: null,
            borders: []
        };
        componentDidMount() {
            this.postCountry();
        }

        postCountry = () => {
          fetch('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code;borders;flag;capital;population;area'

          ).then(response => {
               return response.json();
          }).then(countries => {
              this.setState({countries});
              console.log(countries)
          });

        };

        postInformation = (code) => {
            let countries = this.state.countries;
            let index = countries.findIndex((country) => country.alpha3Code === code);

            let information = {
                name: countries[index].name,
                capital: countries[index].capital,
                flag: countries[index].flag,
                population: countries[index].population,
                area: countries[index].area,
            };
            this.setState({information});

            Promise.all(countries[index].borders.map(border => {
                console.log(border);
                return fetch(`https://restcountries.eu/rest/v2/alpha/${border}`).then(response => {
                    return response.json();
                })
            })).then(borders => {
                this.setState({borders});
                console.log(borders)
            })

        };

        render() {
            return(
                <div>
                    <List countries={this.state.countries} info={this.postInformation} />
                    <Info data={this.state.information} borders={this.state.borders}/>
                </div>
            )
}

}
 export default Country;