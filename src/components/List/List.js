import React from 'react';
import './List.css'

const List = props => {
        return(
            <div className="Countries">
                <ol>
                    {props.countries.map((country, index) =>
                        <li key={index} onClick={() => props.info(country.alpha3Code)}>
                            {country.name}
                            {country.alpha3Code}
                        </li>
                    )}
                </ol>
            </div>
        )
};
export default List;