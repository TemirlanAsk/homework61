import React from 'react';
import './Info.css'

const Info = props => {
    if(props.data) {
        return (
            <div className="Info">
                <b>Name:<span className="Name">{props.data.name}</span></b>
                <b><sapn href={props.data.flag} className="Flag"><img src={props.data.flag}/></sapn></b>
               <b>Capital:  <span className="Capital">{props.data.capital}</span></b>
                <b>Population: <span>{props.data.population}</span></b>
                <b>Area: <span>{props.data.area}</span></b>
                <div>
                    {props.borders.map((bord, index) => <p key={index} >
                        {bord.name}

                    </p>
                    )}
                </div>
            </div>
        )

    }else{
       return(
           <h2 className="Choose">
               choose country
           </h2>
       )
    }
};
export default Info;